# tasksApp
Based on EAN boilerplate.

### Target browsers
* this app targets anything newer than IE9, inluding IE9.

## Prerequisites
* Node.js - Download and Install [Node.js](http://www.nodejs.org/download/). You can also follow [this gist](https://gist.github.com/isaacs/579814) for a quick and easy way to install Node.js
* NPM - Node.js package manager, should be installed when you install node.js.

### Global packages Prerequisites

* Bower - Web package manager, installing [Bower](http://bower.io/) is simple when you have npm:

```
$ npm install -g bower
```
* Grunt - The JavaScript Task Runner, Download and Install [Grunt](http://gruntjs.com) also through npm:

```
$ npm install -g grunt-cli
```

## Additional Packages
* Express - Defined as npm module in the [package.json](package.json) file.
* AngularJS - Defined as bower module in the [bower.json](bower.json) file.
* Twitter Bootstrap - Included in the project

## Build enviroment
There are two preset build environments: "dev", "production"
Which one is used for build determines the "env" field in package.json.
* "dev" should be used for development- it won't minify and won't strip livereload script
* "production" should be used for production-everything get's minified, livereload is removed from index.html

## Quick Install
  The quickest way to get app running is to clone the project and then:

  1.Install dependencies(will call bower install automatically):

    $ npm install

  2. Use [Grunt](https://github.com/gruntjs/grunt-cli) to compile and start the server:

    $ grunt

  3. Then open a browser and go to:

    http://localhost:8080

    8080 is default port specified in config-dev.json


## More Information
  * Contact Jiří Špác on any issue via [E-Mail](mailto:capajj@gmail.com), [Facebook](http://www.facebook.com/capaj), or [Twitter](http://www.twitter.com/capajj).

