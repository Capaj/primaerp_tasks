angular.module('tasksMock',[]).run(function ($httpBackend, storage) {
	var K = 'tasks';
	var id;
	var matchByID = function (it) {
		return it.ID == id;
	};
	var getDate = function (i) {
		var date = new Date();
		date.setDate(date.getDate() + i);
		return date;
	};
	var tasks = storage.get(K);
	if (!tasks) {
		tasks = [
			{ID: 1, name: 'test task 1', project: 'Project 1', due: getDate(1), done: false},
			{ID: 2, name: 'test task 2', project: 'Project 2', due: getDate(2), done: false},
			{ID: 3, name: 'na zkousku 3', project: 'Project 3', due: getDate(3), done: true}
		];
	}

	$httpBackend.when('GET', '/tasks').respond(function (method, url, data, headers) {
		return [200, tasks];
	});

	var re = new RegExp("^/tasks");
	$httpBackend.when('PUT', re).respond(function (method, url, data, headers) {
		id = url.split('/').last;
		var toUpdate = tasks.findOne(matchByID);
		if (toUpdate) {
			tasks.replace(toUpdate, JSON.parse(data));
			storage.set(K, tasks);
			return [200];
		} else {
			return [404];
		}
	});

	$httpBackend.when('DELETE', re).respond(function (method, url, data, headers) {
		id = url.split('/').last;
		var toDelete = tasks.filter(matchByID).first;
		if (toDelete) {
			tasks.remove(toDelete);
			storage.set(K, tasks);
			return [200];
		} else {
			return [204]
		}
	});

	$httpBackend.when('GET', '/tasks/new').respond(function (method, url, data, headers) {
		var newID = tasks.last.ID + 1;
		tasks.push({ID: newID});
		return [200, newID];
	});

	$httpBackend.whenGET(/.*/).passThrough();

});