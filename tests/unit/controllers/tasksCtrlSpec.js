'use strict';
describe('tasksCtrl', function() {
	var scope;
	var ctrl;

	// You need to load modules that you want to test,
	// it loads only the "ng" module by default.
	beforeEach(module('tasksApp'));

	beforeEach(function () {
		module(function ($provide) {
			$provide.value('dialogService', {create: function(){}});
		})
	});

	beforeEach(inject(function ($rootScope, $controller) {
		scope = $rootScope.$new();
		ctrl = $controller('taskEditCtrl', {$scope: scope});
	}));

	it('should be able to save a task', function () {
		expect(scope.save).toBeDefined();
	});

	it('should be able to remove a task', function () {
		expect(scope.remove).toBeDefined();
	});


});