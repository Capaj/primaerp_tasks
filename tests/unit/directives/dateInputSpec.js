// Very basic unit test for a directive with template
describe('Directive: dateInput', function() {
	beforeEach(module('tasksApp'));

	var element, scope;

	beforeEach(module('ngTemplates'));

	beforeEach(inject(function($rootScope, $compile) {
		element = angular.element('<input date-input ng-model="aa">');

		scope = $rootScope;

		$compile(element)(scope);
		scope.$digest();
	}));

	it("should replace to the template", function() {
		expect(element[0].outerHTML).toBe('<input type="date" date-input="" ng-model="aa" class="ng-scope ng-pristine ng-valid">');
	});
});

