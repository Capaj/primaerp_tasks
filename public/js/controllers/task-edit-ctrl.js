app.controller('taskEditCtrl', function ($scope, taskService) {
	var close = function () {
		$scope.dialog.hide();
	};

	$scope.$watch(function () {
		return taskService.currentEdited;
	}, function (cE) {
		$scope.cEtask = cE;
	});

	$scope.remove = function () {
		taskService.remove($scope.cEtask).then(close);
	};

	$scope.save = function () {
		taskService.save($scope.cEtask).then(close)
	};

	$scope.cancel = close;

});