app.controller('rootCtrl', function ($scope, taskService) {
	$scope.TS = taskService;
	$scope.predicate = '-ID';
	$scope.changeOrderBy = function (np) {
		if ($scope.predicate.contains(np)) {
			//order reversal
			if ($scope.predicate[0] == '-') {
				$scope.predicate = np;
			} else {
				$scope.predicate = '-' + np;
			}
		} else {
			//order by p
			$scope.predicate = np;
		}
	};

});
