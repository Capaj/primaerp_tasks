app.service('taskService', function taskService($http, dialogService) {
	var self = this;

	var d = dialogService.create('task_edit', '', 'taskEditCtrl');

	var getData = function (r) {
		return r.data;
	};
	this.getAll = function(){
		return $http.get('/tasks').then(getData);
	};

	this.save = function (task) {
		return $http.put('/tasks/' + task.ID, task);
	};

	this.create = function () {
		return $http.get('/tasks/new').then(function (r) {
			self.edit({ID: r.data});
		});
	};

	this.remove = function (task) {
		return $http.delete('/tasks/' + task.ID).then(function () {
			if (this === self.currentEdited) {
				self.currentEdited = null;
			}
			self.tasks.remove(task);
		});
	};

	this.currentEdited = null;
	this.edit = function (task) {
		d.open();
		self.currentEdited = task;
	};

	this.getAll().then(function (data) {
		self.tasks = data;
	});

});