window.app = angular.module('tasksApp',
    [
        'ngRoute',
        'ngTouch',
        'angularMoment',
        'angularLocalStorage',
		'tasksMock',
        // included, but by default not loaded, if you need it, just add it to script manifest
        'ngMockE2E'
    ]
).config(
    function($locationProvider, $routeProvider) {
        $locationProvider.html5Mode(true);  //Setting HTML5 Location Mode

        routesModule.routes.forEach(function(routeDef){
            $routeProvider.when(routeDef.route, routeDef.resolve);
        });
        $routeProvider.otherwise({redirectTo:'/404'});
    }
);

